package com.wisely.controller;

import com.wisely.domain.WiselyMessage;
import com.wisely.domain.WiselyResonse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.security.Principal;

/**
 * Created by wxw on 2018/12/27.
 */
@Controller
public class WsController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    @MessageMapping("/welcome")
    @SendTo("/topic/getResponse")
    public WiselyResonse say(WiselyMessage message) throws Exception{
        Thread.sleep(3000);
        return new WiselyResonse("Welcome, "+message.getName()+"!");
    }

    @MessageMapping("/chat")
    public void handleChat(Principal principal,String msg){
        if(principal.getName().equals("wyf")){
            messagingTemplate.convertAndSendToUser("wisely","/queue/notifications",
                    principal.getName()+"-send:"+msg);
        }else {
            messagingTemplate.convertAndSendToUser("wyf","/queue/notifications",
                    principal.getName()+"-send:"+msg);
        }
    }
}
