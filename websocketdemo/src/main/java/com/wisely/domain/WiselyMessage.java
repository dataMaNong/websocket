package com.wisely.domain;

/**
 * Created by wxw on 2018/12/27.
 */
public class WiselyMessage {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
