package com.wisely.domain;

/**
 * Created by wxw on 2018/12/27.
 */
public class WiselyResonse {
    private String responseMessage;

    public WiselyResonse() {
    }

    public WiselyResonse(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
